<!DOCTYPE html> 
<html>
	<head>
		<title>De Massagetafel</title>
		<meta name="description" content="website description" />
		<meta name="keywords" content="website keywords, website keywords" />
		<meta http-equiv="content-type" content="text/html; charset=windows-1252" />
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/template.css') }}" />
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/icomoon.css') }}" />
		<script type="text/javascript" src="{{ URL::asset('js/jquery.min.js') }}"></script>
	</head>
	<body>
  		<div id="main">
    		<header>
	  			<div id="banner">
	    			<div id="welcome">
	     				<img src="{{ URL::asset('img/logo.png') }}" width="320" height="100" />
	    			</div>
	    			<div id="welcome_slogan"></div>
	  			</div>
    		</header>
			<nav>
				<div id="menubar">
        			<ul id="nav">
          				<li @if(Route::currentRouteName() == 'home') class="current" @endif 	 ><a href="{{ route('home') }}"		 >Home</a></li>
          				<li @if(Route::currentRouteName() == 'about') class="current" @endif	 ><a href="{{ route('about') }}"	 >Wie ben ik</a></li>
          				<li @if(Route::currentRouteName() == 'treatments') class="current" @endif><a href="{{ route('treatments') }}">Behandelingen</a></li>
          				<li @if(Route::currentRouteName() == 'reviews') class="current" @endif	 ><a href="{{ route('reviews') }}"	 >Recensies</a></li>
          				<li @if(Route::currentRouteName() == 'contact') class="current" @endif	 ><a href="{{ route('contact') }}"	 >Contact</a></li>
        			</ul>
      			</div>
    		</nav>	    
			<div id="site_content">		
				<div class="sidebar_container">       
					<div class="sidebar">
						<div class="sidebar_item">	
                            <a href="https://www.facebook.com/marcel.schaafsma.3" target="_blank">
	   				            <img src="{{ URL::asset('img/fb.png') }}" width="175" height="250">
                            </a>
	   						<iframe src="http://www.facebook.com/plugins/like.php?href=http%3A%2F%2Fwww.facebook.com/Demassagetafel&amp;layout=button_count&amp;show_faces=true&amp;action=like&amp;colorscheme=light&amp" style="overflow:hidden;width:100%;height:80px;" scrolling="no" frameborder="0" allowTransparency="true"><a href="http://www.trivoo.net" class="fbook">www.trivoo.net</a></iframe>       
          				</div>
        			</div>
       			</div>
				<div id="content">
        			<div class="content_item">
						@yield('content')
					</div>
      			</div>
			</div>
  		</div>
    	<footer>
			<ul>
				<li><a href="{{ route('home') }}">Home</a></li>
				<li><a href="{{ route('about') }}">Wie ben ik</a></li>
				<li><a href="{{ route('treatments') }}">Behandelingen</a></li>
				<li><a href="{{ route('reviews') }}">Recensies</a></li>
				<li><a href="{{ route('contact') }}">Contact</a></li>
			</ul>
		</footer>

  
</body>
</html>
