@extends('template')

@section('content')
	<h1>Home</h1> 
	<p>Van harte welkom op de website van De Massagetafel. Wij bieden sportmassage voor sporters en niet - sporters. De praktijk is gespecialiceerd in het gebied van sport en houding en de invoed daar van op het lichaam. Wilt u meteen een afspraak plannen?</p>
	<p>
		Onze praktijk:
		<ul>
			<li>Ligt in Zuid west utrecht Europaplein Citty max toren</li>
			<li>Is goed bereikbaar per auto, fiets en openbaar vervoer</li>
			<li>is 6 dagen per week open (van 's ochtends vroeg tot 's avonds laat).</li>
		</ul>
	</p>
	<p>
		Onze masseurs zijn gediplomeerd. We hanteren de NBSM standaard en dat betekent goed opgeleide sportmasseurs die hun kennis op peil houden en altijd op basis van de laatste stand van de sportmedische wetenschap werken. 
		Een afspraak maken of verdere vragen bel of mail ons!
		Sportieve groet 
		De massagetafel
 	</p>   				  
	<img src="{{ URL::asset('img/massage.jpg') }}" class="margin-top-20" width="400" height="150">
@stop