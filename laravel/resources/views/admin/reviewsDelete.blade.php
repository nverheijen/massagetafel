@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">

			<h1>Recensie verwijderen</h1>

			@if (count($errors) > 0)
				<div class="alert alert-danger">
					<strong>Whoops!</strong> There were some problems with your input.<br><br>
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif
			
			<div class="panel panel-default">
				<div class="panel-heading">Is het weer zo ver...</div>
				<div class="panel-body">
					<div id="delete-review">
						<form action="{{ URL::route('admin.reviews.delete.do', $review->id) }}" method="post">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<p>Weet je <i>heeeeeeel</i> zeker dat je de recensie van <strong>{{ $review->name }}</strong> wilt verwijderen?</p>
							<p>
								Het schreef:
								<span class="message">{{ nl2br($review->message) }}</span>
							</p>
							<ul>
								<li><input type="submit" name="submit" value="Yep, verwijder deze recensie" /></li>
								<li><a class="cancel-link" href="{{ route('admin.reviews') }}">Nope, never mind</a></li>
							</ul>
						</form>
					</div>
				</div>
			</div>
			
			
		</div>
	</div>
</div>
<script src="{{ URL::asset('js/rating.js') }}"></script>
@stop