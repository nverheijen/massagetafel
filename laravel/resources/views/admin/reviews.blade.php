@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">Recensies beheren</div>
				<div class="panel-body">
					@if( $reviews->count() == 0 )
						<p>Er zijn nog geen recencies</p>
					@else
						<div class="reviews-admin">
						@foreach( $reviews as $review )
							<li class="reviews-admin-item">
								<span class="title">
									<span class="name">{{ $review->name }}</span>
									<span class="options">
										<ul>
											<li><a href="{{ route('admin.reviews.delete', $review->id) }}" class="delete">Verwijderen</a></li>
											<li><a href="{{ route('admin.reviews.edit', $review->id) }}" class="edit">Bewerken</a></li>
											@if( $review->approved == "no" )
												<li><a href="{{ route('admin.reviews.approve', $review->id) }}" class="approve">Geef goedkeuring</a></li>
											@else
												<li><a href="{{ route('admin.reviews.disapprove', $review->id) }}" class="disapprove">Verberg</a></li>
											@endif
										</ul>
									</span>
									<span class="meta">
										<span class="date"><span class="inner">{{ $review->created_at }}</span></span>
										<span class="rating">
											<ul>
												@for( $i = 0; $i < $review->rating; $i++ )
													<li><span class="icon-star-full"></span></li>
												@endfor
												@for( $i = 5 - $review->rating; $i > 0; $i-- )
													<li><span class="icon-star-empty"></span></li>
												@endfor
											</ul>
										</span>
									</span>
								</span>
								<span class="message">{{ nl2br($review->message) }}</span>
							</li>
						@endforeach
						</div>
					@endif
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
