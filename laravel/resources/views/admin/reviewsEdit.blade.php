@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			
			<h2>Recensie aanpassen</h2>
			
			@if (count($errors) > 0)
				<div class="alert alert-danger">
					<strong>Whoops!</strong> There were some problems with your input.<br><br>
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif
			
			<form id="review-form" action="{{ URL::route('admin.reviews.edit.do', $review->id) }}" method="post">

				<input type="hidden" name="_token" value="{{ csrf_token() }}">

				<label for="name">Uw naam:</label>
				<input type="text" id="name" name="name" value="{{ $review->name }}" />

				<label for="rating">Rating:</label>
				<input type="hidden" id="rating" name="rating" value="{{ $review->rating }}" />
				<ul id="rating-list" data-locked="yes">
					<li class="star" data-rating="1"><span class="icon-star-empty"></span></li>
					<li class="star" data-rating="2"><span class="icon-star-empty"></span></li>
					<li class="star" data-rating="3"><span class="icon-star-empty"></span></li>
					<li class="star" data-rating="4"><span class="icon-star-empty"></span></li>
					<li class="star last" data-rating="5"><span class="icon-star-empty"></span></li>
					<li id="rating-text">Hoe tevreden was u?</li>
				</ul>

				<label for="message">Recensie:</label>
				<textarea id="message" name="message" rows="10">{{ $review->message }}</textarea>

				<input type="submit" class="submit-link" name="submit" value="Wijzingen opslaan" />
				<a class='cancel-link' href="{{ route('admin.reviews') }}">Cancel</a>

			</form>
			
		</div>
	</div>
</div>
<script src="{{ URL::asset('js/rating.js') }}"></script>
@stop