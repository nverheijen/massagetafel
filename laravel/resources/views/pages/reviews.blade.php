@extends('template')

@section('content')

	<div id="relativity">
		<h1>Recensies</h1>
		<span class="top-right"><a href="{{ route('reviews.write') }}">Laat een recensie achter</a></span>
		@if( $reviews->count() > 0 )
			<ul id="review-lijst">
				@foreach( $reviews as $review )
					<li>
						<span class="title">
							<span class="name">
								{{ $review->name }}
								<span class="date">{{ $review->created_at }}</span>
							</span>
							<span class="rating">
								<ul class="rating-stars">
									@for( $i = 5 - $review->rating; $i > 0; $i-- )
										<li><span class="icon-star-empty"></span></li>
									@endfor
									@for( $i = 0; $i < $review->rating; $i++ )
										<li><span class="icon-star-full"></span></li>
									@endfor
									<li class="rating-text">{{ $reviewTexts[$review->rating] }}</li>
								</ul>
							</span>
						</span>
						<span class="message">
							<?php echo nl2br($review->message); ?>
						</span>
					</li>
				@endforeach
			</ul>
		@else 
			<p>Er zijn nog geen recensies geschreven! Ben jij de eerste?</p>
		@endif
	</div>

@stop