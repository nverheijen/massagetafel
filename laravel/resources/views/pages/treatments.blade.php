@extends('template')

@section('content')

	<h1>Behandelingen</h1>
	
	<h2>Sportmassage  </h2>
	<p>Sport, of het leveren van een sportprestatie, is steeds meer ondenkbaar zonder aanwezigheid van sportmedische begeleiding. De hoge eisen die aan de huidige sportman(vrouw) gesteld worden, vragen naast goede training ook naar goede begeleiding in de vorm van massage en sportverzorging.</p>
	<p>De sportmasseur is de onmisbare steun en toeverlaat voor en na de wedstrijd en training.</p>
	<p>De voordelen van een goede sportmassage op een rij:</p>
	<ul class="lijst">
		<li>Houdt je spieren soepel</li>
		<li>Houdt je lichaam in een goede conditie</li>
		<li>Kan herstel en prestatiebevorderend werken</li>
		<li>Zorgt voor een algehele ontspanning</li>
		<li>Kan blessures helpen voorkomen</li>
		<li>Kan het algemeen welbevinden bevorderen</li>
	</ul>
	
	<h2>Kinesio taping </h2>
	<p>Kinesio Taping is een relatief nieuwe en revolutionaire manier van functioneel tapen. Het is gebaseerd op principes uit diverse disciplines zoals fysiotherapie en kinesiologie. De werking berust op activering van neurologische en circulatoire systemen.</p>
	<p>Spieren zorgen niet alleen voor de beweging van het lichaam, maar regelen ook de veneuze en lymfatische circulatie, de lichaamstemperatuur, etc. Wanneer spieren niet naar behoren werken kan dit leiden tot verschillende soorten klachten. Kinesio Taping versnelt het genezingsproces, geeft pijnvermindering en is onmisbaar bij een actieve behandeling.</p>
	<p>Wereldwijd wordt deze behandelmethode succesvol toegepastdoor fysiotherapeuten, (sport)artsen, medical trainers en sportverzorgers.</p>
		
	<h2>Ontspannende massage</h2>
	<p>Wanneer je wilt genieten van een ontspannende massage, ben je bij De Massagetafel helemaal aan het juiste adres. Gedurende een massage van 25 of 50 minuten, voel je de spanning van je af glijden.</p>
	<p>Je kunt ervoor kiezen een deel van je lichaam te laten masseren, bijvoorbeeld je rug, nek en/of schouders of je kiest evoor om je hele lichaam onder handen te laten nemen. Ook je armen, benen en voeten worden dan in de massage meegenomen. Aan onze massages gaat altijd een korte intake vooraf. Hierdoor krijgen wij een goed beeld van jou en jouw wensen en zo kunnen we de behandeling helemaal op jou afstemmen. Waarom iedereen een ontspanningsmassage kan gebruiken?</p>
	<p>Een goede ontspanningsmassage zorgt ervoor dat je even helemaal loskomt van de dagelijkse stress in lichaam en geest maar het zorgt ook voor:</p>
	<ul class="lijst">
		<li>Bevordering van de bloedcirculatie en stofwisseling</li>
		<li>Bevordering van de afvoer van vocht en afvalstoffen</li>
		<li>Bestrijding van vermoeide spieren, spierspanning en spierkrampen</li>
		<li>Bestrijding van algemene vermoeidheid en spanning.</li>
	</ul>
	
	<h1>Prijslijst</h1>
	<ul id="price-list">
		<li>
			<ul>
				<li>Sportmassage</li>
				<li>Klachtbehandeling</li>
				<li>Ontspannende massage</li>
				<li>Ontspannende massage</li>
			</ul>
		</li>
		<li>
			<ul>
				<li>50 minuten (incl. kinesio taping)</li>
				<li>50 minuten (incl. kinesio taping)</li>
				<li>50 minuten</li>
				<li>25 minuten</li>
			</ul>
		</li>
		<li>
			<ul>
				<li><strong>&euro; 35,00</strong></li>
				<li><strong>&euro; 35,00</strong></li>
				<li><strong>&euro; 30,00</strong></li>
				<li><strong>&euro; 17,50</strong></li>
			</ul>
		</li>
	</ul>

@stop