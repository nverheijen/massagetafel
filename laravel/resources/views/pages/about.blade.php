@extends('template')

@section('content')

	<h1>Over mij</h1> 
    
	<p>Mijn naam is Marcel Schaafsma huidig derde jaar’s fysiotherapie student bij de HU. Ik ben begonnen in de gezondheid sector als een fitness instructeur en later voor me zelf begonnen als personal trainer met mijn eigen bedrijf’ ”achieving goals’’.</p>
<p>Vervolgens ben ik gaan stage lopen bij Fysio-Emmermeer. Na 1 jaar stage ben ik daar gaan werken als assistent-fysiotherapeut . Mijn functie daar was met name het trainen van patiënten zo wel in de fitness als in de praktijk. Daar naast heb ik mijn diploma als sportmasseur gehaald en was ik voor me zelf begonnen als sportmasseur aan huis.</p>
	<p>Eenmaal in Utrecht ben ik voor me zelf begonnen als masseur voor mijn bedrijfje “De massage tafel’’ en dat doe ik nu al met 3 jaar met veel plezier. Ik heb altijd veel interesse gehad op het gebied van herstel en overbelasting en daar heb ik mij op gefocust de afgelopen jaren. </p>   		
		  
	<div class="content_imagetext">
		<div class="content_image">
			<img src="{{ URL::asset('img/marcel.jpg') }}" alt="marcel" width="150"/>
		</div>
		<p>Sportieve groet, Marcel Schaafsma</p>
	</div>

@stop