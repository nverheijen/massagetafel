@extends('template')

@section('content')

	<h1>Schrijf een recensie</h1>
	
	<form id="review-form" action="{{ URL::route('reviews.write.do') }}" method="post">
		
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				<p><strong>Whoops!</strong> Het volgende is mis gegaan:</p>
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
			
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		
		<label for="name">Uw naam:</label>
		<input type="text" id="name" name="name" value="{{ $old_name }}" />
		
		<label for="rating">Rating:</label>
		<input type="hidden" id="rating" name="rating" value="{{ $old_rating }}" />
		<ul id="rating-list" data-locked="yes">
			<li class="star" data-rating="1"><span class="icon-star-empty"></span></li>
			<li class="star" data-rating="2"><span class="icon-star-empty"></span></li>
			<li class="star" data-rating="3"><span class="icon-star-empty"></span></li>
			<li class="star" data-rating="4"><span class="icon-star-empty"></span></li>
			<li class="star last" data-rating="5"><span class="icon-star-empty"></span></li>
			<li id="rating-text">Hoe tevreden was u?</li>
		</ul>
		
		<label for="message">Recensie:</label>
		<textarea id="message" name="message" rows="10">{{ $old_message }}</textarea>
		
		<input type="submit" name="submit" value="Plaats recensie" />
		
	</form>

	<script src="{{ URL::asset('js/rating.js') }}"></script>

@stop