@extends('template')

@section('content')

	<div id="contact-form" class="form_settings">	
		
		<h1>Contact</h1>
        
        <p>
            <strong style="width: 150px; display: inline-block;">Telefoon nummer:</strong>
            06 52 62 05 35
        </p>
        <p>
            <strong style="width: 150px; display: inline-block;">Adres:</strong>
            Europaplein 730, 3526 WP Utrecht
        </p>
        <p>
            U kunt ons ook een bericht sturen via ons contact formulier:
        </p>
		<form action="{{ route('contact.send') }}" method="post">
			
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			
			<label for="name">Uw naam:</label>
			<input type="text" id="name" name="name" />
			
			<label for="email">Uw email:</label>
			<input type="email" id="email" name="email" />
			
			<label for="subject">Onderwerp:</label>
			<input type="text" id="subject" name="subject" />
			
			<label for="message">Uw bericht:</label>
			<textarea id="message" name="message"></textarea>
			
			<input class="submit" type="submit" name="submit" value="Send" />
			
		</form>
		
 	</div>

@stop