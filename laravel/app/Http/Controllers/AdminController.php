<?php namespace App\Http\Controllers;

use Request;
use App\Review;

class AdminController extends Controller {
	
	public function __construct(){
		$this->middleware('auth');
	}
	
	public function dashboard()
	{
		return view('admin.dashboard');
	}
	
	public function reviews()
	{
		$reviews = Review::orderBy('id', 'DESC')->get();
		return view('admin.reviews', array('reviews' => $reviews));	
	}
	
	public function reviewsDelete($id)
	{
		$review = Review::find($id);
		return view('admin.reviewsDelete', array('review' => $review));
	}
	
	public function reviewsDeleteDo($id)
	{
		$review = Review::find($id);
		if( $review->delete() )
		{
			return redirect()->route('admin.reviews');
		}
		else
		{
			return redirect()->back();	
		}
	}
	
	public function reviewsEdit($id)
	{
		$review = Review::find($id);
		return view('admin.reviewsEdit', array('review' => $review));
	}
	
	public function reviewsEditDo($id)
	{
		$review = Review::find($id);
		$review->name = Request::input('name');
		$review->rating = Request::input('rating');
		$review->message = Request::input('message');
		
		if( $review->save() )
		{
			return redirect()->route('admin.reviews');
		}
		else
		{
			return redirect()->back()->withInput();	
		}
	}
	
	public function reviewsApprove($id)
	{
		$review = Review::find($id);
		$review->approved = 1;
		$review->save();
		return redirect()->route('admin.reviews');	
	}
	
	public function reviewsDisapprove($id)
	{
		$review = Review::find($id);
		$review->approved = 0;
		$review->save();
		return redirect()->route('admin.reviews');
	}
	
}