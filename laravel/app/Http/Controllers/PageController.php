<?php namespace App\Http\Controllers;

use Request;
use Validator;
use Illuminate\Support\MessageBag;
use App\Review;

class PageController extends Controller {
	
	public function __construct()
	{
		
	}
	
	public function about()
	{
		return view('pages.about');
	}
	
	public function treatments()
	{
		return view('pages.treatments');
	}
	
	public function reviews()
	{
		$reviews = Review::where("approved", "=", "1")->orderBy('id', 'DESC')->get();
		
		$reviewTexts = array(
			0 => "Niks ingevuld",
			1 => "Niet tevreden",
			2 => "Wel tevreden",
			3 => "Tevreden",
			4 => "Erg tevreden",
			5 => "Echt FUCKING tevreden!"	
		);
		
		return view('pages.reviews', array('reviews' => $reviews, 'reviewTexts' => $reviewTexts));
	}
	
	public function reviewsWrite()
	{
		return view('pages.reviewsWrite', array(
			'old_name'		=> Request::old('name'),
			'old_rating'	=> Request::old('rating'),
			'old_message'	=> Request::old('message')
		));	
	}
	
	public function reviewsWriteDo()
	{	
		$name    = Request::input('name');
		$rating  = Request::input('rating');
		$message = Request::input('message');
		
		$validator = Validator::make(
			array(
				'name' 	  	=> $name,
				'rating'  	=> $rating,
				'message' 	=> $message
			),
			array(
				'name' 		=> 'required',
				'rating'	=> 'required|integer|min:1',
				'message'	=> 'required'
			),
			array(
				'required' => 'Het :attribute veld is niet ingevuld.',
				'min'	   => 'De :attribute moet minimaal 1 ster zijn.'
			)
		);
		
		if( $validator->fails() )
		{
			return redirect()->route('reviews.write')->withInput()->withErrors($validator->errors());	
		}
		else
		{
			$review = new Review;
			$review->name 		= $name;
			$review->rating 	= $rating;
			$review->message 	= $message;
			$review->approved 	= 0;

			if( $review->save() )
			{
				return redirect()->route('reviews.thanks');	
			}
			else
			{
				$bag = new MessageBag;
				$bag->add('system', 'Could not save the review.');
				return redirect()->route('reviews.write')->withInput()->withErrors($bag->messages());
			}
		}
		
	}
	
	public function reviewsThanks()
	{
		return view('pages.reviewsThanks');	
	}
	
	public function contact()
	{
		return view('pages.contact');
	}
	
	public function contactSend()
	{
		$naam    = Request::input('name');
		$email   = Request::input('email');
		$subject = Request::input('subject');
		$msg	 = Request::input('message');
	
		// echo $naam."<br>".$email."<br>".$subject."<br>".$message;										   
	
		$to 		= "info@demassagetafel.nl";
		$from 		= "contact-formulier@demassagetafel.nl";
	
		$headers = "From: " . $from . "\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "X-Priority: 3\r\n";
		$headers .= "X-MSMail-Priority: Normal\r\n";    
		$headers .= "X-Mailer: PHP/" . phpversion();
		
		$message = "
			Het volgende is ingevuld in het contact formulier:\n\n
			Naam: ".$naam."\n
			Email: ".$email."\n
			Onderwerp: ".$subject."\n
			Bericht:\n\n
			".$msg;
	
		if( mail($to, $subject, $message, $headers) )
		{
			header('Location: '.route('contact.thanks'));
		}
	}
	
	public function contactThanks()
	{
		return view('pages.contactThanks');	
	}
	
}


