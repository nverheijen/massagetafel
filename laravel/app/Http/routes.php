<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('admin', 						['as' => 'admin.dashboard', 			'uses' => 'AdminController@dashboard']);
Route::get('admin/reviews', 				['as' => 'admin.reviews', 				'uses' => 'AdminController@reviews']);
Route::get('admin/reviews/delete/{id}',		['as' => 'admin.reviews.delete',		'uses' => 'AdminController@reviewsDelete']);
Route::post('admin/reviews/delete/{id}/do',	['as' => 'admin.reviews.delete.do',		'uses' => 'AdminController@reviewsDeleteDo']);
Route::get('admin/reviews/edit/{id}',		['as' => 'admin.reviews.edit',			'uses' => 'AdminController@reviewsEdit']);
Route::post('admin/reviews/edit/{id}/do',	['as' => 'admin.reviews.edit.do',		'uses' => 'AdminController@reviewsEditDo']);
Route::get('admin/reviews/approve/{id}',	['as' => 'admin.reviews.approve',		'uses' => 'AdminController@reviewsApprove']);
Route::get('admin/reviews/disapprove/{id}',	['as' => 'admin.reviews.disapprove',	'uses' => 'AdminController@reviewsDisapprove']);

Route::controllers([
	'auth' 		=> 'Auth\AuthController',
	'password'  => 'Auth\PasswordController',
]);

Route::get('/', 							['as' => 'home', 				'uses' => 'WelcomeController@splash']);
Route::get('/over-mij', 					['as' => 'about', 				'uses' => 'PageController@about']);
Route::get('/behandelingen', 				['as' => 'treatments',			'uses' => 'PageController@treatments']);
Route::get('/recensies',					['as' => 'reviews',				'uses' => 'PageController@reviews']);
Route::get('/contact',						['as' => 'contact',				'uses' => 'PageController@contact']);
Route::post('/contact-send',				['as' => 'contact.send',		'uses' => 'PageController@contactSend']);
Route::get('/contact-thanks',				['as' => 'contact.thanks',		'uses' => 'PageController@contactThanks']);
Route::get('/recencies-schrijven', 			['as' => 'reviews.write', 		'uses' => 'PageController@reviewsWrite']);
Route::post('/recensies-schrijven-plaatsen',['as' => 'reviews.write.do',	'uses' => 'PageController@reviewsWriteDo']);
Route::get('/recensies-schrijven-voltooid', ['as' => 'reviews.thanks', 		'uses' => 'PageController@reviewsThanks']);
