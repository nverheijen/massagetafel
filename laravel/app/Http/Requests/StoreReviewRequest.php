<?php namespace App\Http\Requests;

class StoreReviewRequest extends Request {
	
	public function rules()
	{
		return [
			'name'		=> 'required',
			'rating'	=> 'required|integer|min:1',
			'message'	=> 'required'
		];
	}
	
	public function authorize()
	{
		return true;	
	}
	
}