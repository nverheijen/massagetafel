<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model {
	
	protected $table    = "reviews";
	protected $guarded  = array('id', 'created_at', 'updated_at');
	protected $fillable = array('name', 'rating', 'message', 'approved');
	
	
	
}