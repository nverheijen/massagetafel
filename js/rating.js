$(document).ready(function(){
	
	var rating = $("#rating").val();
	$(".star").each(function(){
		var currentRating = $(this).data('rating');
		if( currentRating <= rating ){
			$(this).find('span').removeClass('icon-star-empty').addClass('icon-star-full');	
		}
	});
	
	var ratingTexts = [
		"Niet tevreden",
		"Matig",
		"Tevreden",
		"Zeer tevreden",
		"Perfect"
	];
		
	$(".star").hover(
		function(){
			var rating = $(this).data('rating');
			highlight(rating);
		}, function(){
			unhighlight();
		}
	);
		
	$(".star").click(function(){
		$("#rating-list").data('locked', 'yes');
		$("#rating").val($(this).data('rating'));
	});
		
	function highlight( rating ){
		$(".star").each(function(){
			var currentRating = $(this).data('rating');
			if( currentRating <= rating ) $(this).find('span').removeClass('icon-star-empty').addClass('icon-star-full');
			if( currentRating == rating ){
				var textIndex = currentRating - 1;
				$("#rating-text").html(ratingTexts[textIndex]);
			}
		});
	}
		
	function unhighlight(){
		var locked = $("#rating-list").data('locked');
		if($("#rating-list").data('locked') === 'no'){
			$(".star").each(function(){
				$(this).find('span').removeClass('icon-star-full').addClass('icon-star-empty');
			});
			$("#rating-text").html("Hoe tevreden was u?");
		} else {
			var rating = $("#rating").val();
			$(".star").each(function(){
				var currentRating = $(this).data('rating');
				if( currentRating > rating ){
					$(this).find('span').removeClass('icon-star-full').addClass('icon-star-empty');	
				}
			});
		}
	}
		
});